program project1;
//UWAGA!!!! Zakladamy ze listy moga miec tylko jeden element o tej samej wartosci
type
wsk_wierzcholki=^wierzcholki;
wsk_wsk_wierzcholki=^wsk_wierzcholki;
wsk_drogi=^drogi;
wsk_wsk_drogi=^wsk_drogi;
wierzcholki= record
      numer: integer;
      nast:wsk_wierzcholki;
      aktOdleglosci:integer;
      drogaDoInnychWierzcholkow:wsk_drogi;
      end;
drogi=record
      waga:integer;
      drogaDo:wsk_wierzcholki;
      nast:wsk_drogi;
end;
procedure add(liczba:integer;aktOdleglosci:integer;glowa:wsk_wsk_wierzcholki);
var tmp:wsk_wierzcholki;
begin
  New(tmp);
  tmp^.numer:=liczba;
  tmp^.aktOdleglosci:=aktOdleglosci;
  tmp^.nast:=glowa^;
  tmp^.drogaDoInnychWierzcholkow:=NIL;
  glowa^:=tmp;
end;

procedure addDroga(waga:integer;glowa:wsk_wsk_drogi;drogaDo:wsk_wierzcholki);
var tmp:wsk_drogi;
begin
  New(tmp);
  tmp^.nast:=glowa^;
  tmp^.drogaDo:=drogaDo;
  tmp^.waga:=waga;
  glowa^:=tmp;
end;

function search(liczba:integer;glowa:wsk_wsk_wierzcholki):wsk_wierzcholki;   //Wyszukuje element w liscie, jeżeli nie ma to zwraca wskaznik NIL
var tmp:wsk_wierzcholki;
begin
    tmp:=glowa^;
    result:=NIL;
    while tmp^.nast<>Nil do
    begin
         if tmp^.numer=liczba then
         begin
           result:=tmp;
         end;
    tmp:=tmp^.nast;
    end;
end;

procedure wyswietlDroga(glowa:wsk_wsk_drogi);
var tmp:wsk_drogi;
begin
  tmp:=glowa^;
  while tmp^.nast<>Nil do
  begin
    writeln(tmp^.waga,' ',tmp^.drogaDo^.numer);
    tmp:=tmp^.nast;
  end;
end;

procedure wyswietl(glowa:wsk_wsk_wierzcholki);
var tmp:wsk_wierzcholki;
begin
  tmp:=glowa^;
  while tmp^.nast<>Nil do
  begin
    if tmp^.aktOdleglosci=100000 then WriteLn('Nie ma drogi!')
    else
    writeln(tmp^.numer,' ',tmp^.aktOdleglosci);
    tmp:=tmp^.nast;
  end;
end;
procedure niszczElement(liczba:integer;glowa:wsk_wsk_wierzcholki);
var tmp:wsk_wierzcholki;
var tmp_prev:wsk_wierzcholki;
var pom:wsk_wierzcholki;
var pom2:integer;
begin
  tmp:=glowa^;
  tmp_prev:=NIL;
  pom2:=0;
  while tmp^.nast<>Nil do
  begin
    if tmp^.numer=liczba then
    begin
        if tmp_prev=NIL then
        begin
          pom:=tmp;
          glowa^:=tmp^.nast;
          tmp:=tmp^.nast;
          if tmp^.nast=Nil then pom2:=1;
          Dispose(pom);
        end
        else
        begin
          pom:=tmp;
          tmp_prev^.nast:=tmp^.nast;
          tmp:=tmp^.nast;
          if tmp^.nast=Nil then tmp:=tmp_prev;
          Dispose(pom);
        end;
    end;
    if pom2=1 then Break;
    tmp_prev:=tmp;
    tmp:=tmp^.nast;
  end;
end;
function isEmpty(glowa:wsk_wsk_wierzcholki):Boolean;
begin
  result:=False;
    if glowa^^.nast=nil then result:=True;
end;
procedure niszcz(glowa:wsk_wsk_wierzcholki);
   var tmp,tmp2:wsk_wierzcholki;
begin
  tmp:=glowa^;
  while tmp^.nast<>Nil do
  begin
    tmp2:=tmp;
    tmp:=tmp^.nast;
    Dispose(tmp2);
  end;
end;
procedure algorytmDijkstry(glowa:wsk_wsk_wierzcholki;start:integer);
   var tmp,tmp2,v,v2,u:wsk_wierzcholki;
   var tmp3:wsk_drogi;
   var doSprawdzeniaWierzcholki:wsk_wierzcholki;
   var minWarAktOdleglosci:integer;
begin
  New(doSprawdzeniaWierzcholki);
  tmp:=glowa^;
  search(start,glowa)^.aktOdleglosci:=0;
  //dodajemy wierzcholki do listy doSprawdzenia
  tmp2:=glowa^;
  while tmp2^.nast<>Nil do
  begin
    add(tmp2^.numer,tmp2^.aktOdleglosci,@doSprawdzeniaWierzcholki);
    doSprawdzeniaWierzcholki^.drogaDoInnychWierzcholkow:=tmp2^.drogaDoInnychWierzcholkow;
    tmp2:=tmp2^.nast;
  end;
  while isEmpty(@doSprawdzeniaWierzcholki)=False do
  begin
              minWarAktOdleglosci:=1000000;
            //szukamy wierzcholka w doSprawdzeniaWierzcholki z minimalna wartoscia
            tmp2:=doSprawdzeniaWierzcholki;
            v:=doSprawdzeniaWierzcholki;//zakladamy ze glowa jest wierzcholkiem z min aktoodleglosci
            v2:=search(doSprawdzeniaWierzcholki^.numer,glowa);
            while tmp2^.nast<>Nil do
            begin
            //wyswietl(glowa);
            if search(tmp2^.numer,glowa)^.aktOdleglosci<minWarAktOdleglosci then
            begin
            v:=tmp2;
            v2:=search(tmp2^.numer,glowa);
            minWarAktOdleglosci:=search(tmp2^.numer,glowa)^.aktOdleglosci;
            end;
            tmp2:=tmp2^.nast;
            end;
            tmp3:=v2^.drogaDoInnychWierzcholkow;
            niszczElement(v2^.numer,@doSprawdzeniaWierzcholki);//usuwamy element z listy
            while tmp3^.nast<>NIL do
            begin
            //writeln(tmp3^.drogaDo^.numer,' ',tmp3^.drogaDo^.aktOdleglosci);
            if search(tmp3^.drogaDo^.numer,@doSprawdzeniaWierzcholki)<>NIL then
            begin
              if tmp3^.drogaDo^.aktOdleglosci>v2^.aktOdleglosci+tmp3^.waga then
              begin
             // wyswietl(glowa);
              tmp3^.drogaDo^.aktOdleglosci:=v2^.aktOdleglosci+tmp3^.waga;
              //v2^.aktOdleglosci:=v2^.aktOdleglosci+tmp3^.waga;
              //v2^.aktOdleglosci:=v2^.aktOdleglosci;
              end;
              end;
            tmp3:=tmp3^.nast;
            end;
            end;
end;

var Plik:Text;
var x:Integer;
var lWierzcholkow,lKrawedzi:integer;
var glowa:wsk_wierzcholki;
var a,b,start,waga:integer;
var pom:wsk_wsk_drogi;
var pom2:wsk_wierzcholki;
begin
  Assign(plik, 'graf1.txt');
  Reset(plik);
  Read(plik,lWierzcholkow,lKrawedzi,start);
  New(glowa);

  for x:=1 to lWierzcholkow  do
  begin
  add(x,1000000,@glowa);
  New(glowa^.drogaDoInnychWierzcholkow);
  glowa^.drogaDoInnychWierzcholkow^.nast:=nil;
  end;
  for x:=1 to lKrawedzi  do
  begin
    Read(plik,a,b,waga);
    pom2:=search(a,@glowa);
    pom:=@pom2^.drogaDoInnychWierzcholkow;
    addDroga(waga,pom,search(b,@glowa));
  end;
  algorytmDijkstry(@glowa,start);
  wyswietl(@glowa);
  niszcz(@glowa);
  ReadLn;
end.
